# Game of Life

The Game of Life, also known simply as Life, is a cellular automaton devised by the British mathematician John Horton Conway in 1970.

The rules are simple: Each cell has two states – alive or dead, and interacts with the eight cells surrounding it.

- If a live cell has 2 to 3 live neighbors, it remains alive in the next stage; otherwise, it dies.
- If a dead cell has exactly 3 live neighbors, it becomes a live cell in the next stage; otherwise, it stays dead.

Conway’s Game of Life is a classic example of how simple rules can produce complex patterns. During the evolution, one can observe some very beautiful transformations and elegant geometric shapes.
