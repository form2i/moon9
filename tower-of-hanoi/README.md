# Tower of Hanoi

This is a widely circulated story, a legend about the end of the world.

It is also a classic example used in learning recursion, basically every resource that explains recursive algorithms mentions it.

There are various game versions implemented, serving as a puzzle game similar to the Chinese traditional game of “Huarong Dao” (the Flowery Path).

Here is a version that includes more features, such as an automatic solution.
