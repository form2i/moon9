# Moon9

A collection of simple projects.

## 1. moon9

A simple game to guess the number, written when I first started learning JavaScript. The code is quite ugly.

People always find their past work embarrassing at some point. Although it’s ugly, it’s still something I created. And the fact that I find it ugly now means that I’ve improved somewhat, and I shouldn’t feel discouraged by progress.

[Live Demo ↗](https://mirreal.github.io/moon9/moon9.html)

## 2. Game of Life

A game invented by the mathematician Conway with simple rules that produce wonderful transformations.

[Live Demo ↗](https://mirreal.github.io/moon9/game-of-life/)

## 3. Tower of Hanoi

A version of the Tower of Hanoi game with a visual solution demonstration.

[Live Demo ↗](https://mirreal.github.io/moon9/tower-of-hanoi/)
